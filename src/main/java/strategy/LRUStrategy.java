package strategy;

public class LRUStrategy<T> extends StrategyAbstract<T> {

    @Override
    public void putObject(T key) {
        getObjectsStorage().put(key, System.nanoTime());
    }
}
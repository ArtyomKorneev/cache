package strategy;

import java.util.Map;
import java.util.TreeMap;

public abstract class StrategyAbstract<T> {
    private final Map<T, Long> objectsStorage;
    private final TreeMap<T, Long> sortedObjectsStorage;

    StrategyAbstract() {
        this.objectsStorage = new TreeMap<>();
        this.sortedObjectsStorage = new TreeMap<>(new ComparatorImpl<>(objectsStorage));
    }

    Map<T, Long> getObjectsStorage() {
        return objectsStorage;
    }

    TreeMap<T, Long> getSortedObjectsStorage() {
        return sortedObjectsStorage;
    }

    public abstract void putObject(T key);

    public void removeObject(T key) {
        if (isObjectPresent(key)) {
            objectsStorage.remove(key);
        }
    }

    public boolean isObjectPresent(T key) {
        return objectsStorage.containsKey(key);
    }

    public T getReplacedKey() {
        sortedObjectsStorage.putAll(objectsStorage);
        return sortedObjectsStorage.firstKey();
    }

    public void clear() {
        objectsStorage.clear();
    }
}

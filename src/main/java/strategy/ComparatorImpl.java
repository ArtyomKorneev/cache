package strategy;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Map;

class ComparatorImpl<T> implements Comparator<T>, Serializable {
    private static final long serialVersionUID = 1873867581022399812L;

    private final Map<T, Long> comparatorMap;

    ComparatorImpl(Map<T, Long> comparatorMap) {
        this.comparatorMap = comparatorMap;
    }

    @Override
    public int compare(T key1, T key2) {
        Long key1Long = comparatorMap.get(key1);
        Long key2Long = comparatorMap.get(key2);
        if (key1Long < key2Long) {
            return -1;
        }
        if (key1Long > key2Long) {
            return 1;
        }
        return 0;
    }
}

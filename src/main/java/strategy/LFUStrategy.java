package strategy;

public class LFUStrategy<T> extends StrategyAbstract<T> {

    @Override
    public void putObject(T key) {
        long frequency = 1;
        if (getObjectsStorage().containsKey(key)) {
            frequency = getObjectsStorage().get(key) + 1;
        }
        getObjectsStorage().put(key, frequency);
    }
}

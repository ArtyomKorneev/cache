package strategy;

public class MRUStrategy<T> extends StrategyAbstract<T> {

    @Override
    public void putObject(T key) {
        getObjectsStorage().put(key, System.nanoTime());
    }

    @Override
    public T getReplacedKey() {
        getSortedObjectsStorage().putAll(getObjectsStorage());
        return getSortedObjectsStorage().lastKey();
    }
}

package strategy;

public enum StrategyType {
    LRU,
    MRU,
    LFU
}

package cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import strategy.LFUStrategy;
import strategy.LRUStrategy;
import strategy.MRUStrategy;
import strategy.StrategyAbstract;
import strategy.StrategyType;

import java.io.IOException;
import java.io.Serializable;

import static java.lang.String.format;

public class Cache<K extends Serializable, V extends Serializable> implements CacheI<K, V> {
    private static final Logger log = LoggerFactory.getLogger(Cache.class);

    private final MemoryStore<K, V> firstLevelCache;
    private final FileSystemStore<K, V> secondLevelCache;
    private final StrategyAbstract<K> strategy;

    public Cache(final int memoryCapacity, final int fileCapacity, final StrategyType strategyType) throws IOException {
        this.firstLevelCache = new MemoryStore<>(memoryCapacity);
        this.secondLevelCache = new FileSystemStore<>(fileCapacity);
        this.strategy = getStrategy(strategyType);
    }

    public Cache(final int memoryCapacity, final int fileCapacity) throws IOException {
        this.firstLevelCache = new MemoryStore<>(memoryCapacity);
        this.secondLevelCache = new FileSystemStore<>(fileCapacity);
        this.strategy = getStrategy(StrategyType.LFU);
    }

    MemoryStore<K, V> getFirstLevelCache() {
        return firstLevelCache;
    }

    FileSystemStore<K, V> getSecondLevelCache() {
        return secondLevelCache;
    }

    StrategyAbstract<K> getStrategy() {
        return strategy;
    }

    private StrategyAbstract<K> getStrategy(StrategyType strategyType) {
        switch (strategyType) {
            case LRU:
                return new LRUStrategy<>();
            case MRU:
                return new MRUStrategy<>();
            case LFU:
            default:
                return new LFUStrategy<>();
        }
    }

    @Override
    public synchronized void put(K newKey, V newValue) throws IOException {
        if (firstLevelCache.isObjectPresent(newKey) || firstLevelCache.hasEmptyPlace()) {
            log.debug(format("Put object with key %s to the 1st level", newKey));
            firstLevelCache.put(newKey, newValue);
            if (secondLevelCache.isObjectPresent(newKey)) {
                secondLevelCache.remove(newKey);
            }
        } else if (secondLevelCache.isObjectPresent(newKey) || secondLevelCache.hasEmptyPlace()) {
            log.debug(format("Put object with key %s to the 2nd level", newKey));
            secondLevelCache.put(newKey, newValue);
        } else {
            replaceObject(newKey, newValue);
        }

        if (!strategy.isObjectPresent(newKey)) {
            log.debug(format("Put object with key %s to strategy", newKey));
            strategy.putObject(newKey);
        }
    }

    private void replaceObject(K key, V value) throws IOException {
        K replacedKey = strategy.getReplacedKey();
        if (firstLevelCache.isObjectPresent(replacedKey)) {
            log.debug(format("Replace object with key %s from 1st level", replacedKey));
            firstLevelCache.remove(replacedKey);
            firstLevelCache.put(key, value);
        } else if (secondLevelCache.isObjectPresent(replacedKey)) {
            log.debug(format("Replace object with key %s from 2nd level", replacedKey));
            secondLevelCache.remove(replacedKey);
            secondLevelCache.put(key, value);
        }
    }

    @Override
    public synchronized V get(K key) {
        if (firstLevelCache.isObjectPresent(key)) {
            strategy.putObject(key);
            return firstLevelCache.get(key);
        } else if (secondLevelCache.isObjectPresent(key)) {
            strategy.putObject(key);
            return secondLevelCache.get(key);
        }
        return null;
    }

    @Override
    public synchronized void remove(K key) {
        if (firstLevelCache.isObjectPresent(key)) {
            log.debug(format("Remove object with key %s from 1st level", key));
            firstLevelCache.remove(key);
        }
        if (secondLevelCache.isObjectPresent(key)) {
            log.debug(format("Remove object with key %s from 2nd level", key));
            secondLevelCache.remove(key);
        }
        strategy.removeObject(key);
    }

    @Override
    public int size() {
        return firstLevelCache.size() + secondLevelCache.size();
    }

    @Override
    public boolean isObjectPresent(K key) {
        return firstLevelCache.isObjectPresent(key) || secondLevelCache.isObjectPresent(key);
    }

    @Override
    public void clearCache() throws IOException {
        firstLevelCache.clearCache();
        secondLevelCache.clearCache();
        strategy.clear();
    }

    @Override
    public synchronized boolean hasEmptyPlace() {
        return firstLevelCache.hasEmptyPlace() || secondLevelCache.hasEmptyPlace();
    }
}

package cache;

import java.io.IOException;

public interface CacheI<K, V> {
    void put(K key, V value) throws IOException;

    V get(K key);

    void remove(K key);

    int size();

    boolean isObjectPresent(K key);

    boolean hasEmptyPlace();

    void clearCache() throws IOException;
}

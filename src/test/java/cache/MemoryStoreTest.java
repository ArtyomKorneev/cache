package cache;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class MemoryStoreTest {
    private static final String VALUE1 = "value1";
    private static final String VALUE2 = "value2";

    private MemoryStore<Integer, String> memoryCache;

    @Before
    public void init() {
        memoryCache = new MemoryStore<>(3);
    }

    @After
    public void clearCache() {
        memoryCache.clearCache();
    }

    @Test
    public void shouldPutGetAndRemoveObjectTest() {
        memoryCache.put(0, VALUE1);
        assertEquals(VALUE1, memoryCache.get(0));
        assertEquals(1, memoryCache.size());

        memoryCache.remove(0);
        assertNull(memoryCache.get(0));
    }

    @Test
    public void shouldNotGetObjectFromCacheIfNotExistsTest() {
        memoryCache.put(0, VALUE1);
        assertEquals(VALUE1, memoryCache.get(0));
        assertNull(memoryCache.get(111));
    }

    @Test
    public void shouldNotRemoveObjectFromCacheIfNotExistsTest() {
        memoryCache.put(0, VALUE1);
        assertEquals(VALUE1, memoryCache.get(0));
        assertEquals(1, memoryCache.size());

        memoryCache.remove(5);
        assertEquals(VALUE1, memoryCache.get(0));
    }

    @Test
    public void shouldGetCacheSizeTest() {
        memoryCache.put(0, VALUE1);
        assertEquals(1, memoryCache.size());

        memoryCache.put(1, VALUE2);
        assertEquals(2, memoryCache.size());
    }

    @Test
    public void isObjectPresentTest() {
        assertFalse(memoryCache.isObjectPresent(0));

        memoryCache.put(0, VALUE1);
        assertTrue(memoryCache.isObjectPresent(0));
    }

    @Test
    public void isEmptyPlaceTest() {
        memoryCache = new MemoryStore<>(5);

        IntStream.range(0, 4).forEach(i -> memoryCache.put(i, "String " + i));

        assertTrue(memoryCache.hasEmptyPlace());
        memoryCache.put(5, "String");
        assertFalse(memoryCache.hasEmptyPlace());
    }

    @Test
    public void shouldClearCacheTest() {
        IntStream.range(0, 3).forEach(i -> memoryCache.put(i, "String " + i));

        assertEquals(3, memoryCache.size());
        memoryCache.clearCache();
        assertEquals(0, memoryCache.size());
    }
}

package cache;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import strategy.StrategyType;

import java.io.IOException;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class CacheTest {
    private static final Logger log = LoggerFactory.getLogger(CacheTest.class);

    private static final String VALUE1 = "value1";
    private static final String VALUE2 = "value2";
    private static final String VALUE3 = "value3";

    private Cache<Integer, String> twoLevelCache;

    @Before
    public void init() throws IOException {
        twoLevelCache = new Cache<>(1, 1);
    }

    @After
    public void clearCache() throws IOException {
        twoLevelCache.clearCache();
    }

    @Test
    public void shouldPutGetAndRemoveObjectTest() throws IOException {
        twoLevelCache.put(0, VALUE1);
        assertEquals(VALUE1, twoLevelCache.get(0));
        assertEquals(1, twoLevelCache.size());

        twoLevelCache.remove(0);
        assertNull(twoLevelCache.get(0));
    }

    @Test
    public void shouldRemoveObjectFromFirstLevelTest() throws IOException {
        twoLevelCache.put(0, VALUE1);
        twoLevelCache.put(1, VALUE2);

        assertEquals(VALUE1, twoLevelCache.getFirstLevelCache().get(0));
        assertEquals(VALUE2, twoLevelCache.getSecondLevelCache().get(1));

        twoLevelCache.remove(0);

        assertNull(twoLevelCache.getFirstLevelCache().get(0));
        assertEquals(VALUE2, twoLevelCache.getSecondLevelCache().get(1));
    }

    @Test
    public void shouldRemoveObjectFromSecondLevelTest() throws IOException {
        twoLevelCache.put(0, VALUE1);
        twoLevelCache.put(1, VALUE2);

        assertEquals(VALUE1, twoLevelCache.getFirstLevelCache().get(0));
        assertEquals(VALUE2, twoLevelCache.getSecondLevelCache().get(1));

        twoLevelCache.remove(1);

        assertEquals(VALUE1, twoLevelCache.getFirstLevelCache().get(0));
        assertNull(twoLevelCache.getSecondLevelCache().get(1));
    }

    @Test
    public void shouldNotGetObjectFromCacheIfNotExistsTest() throws IOException {
        twoLevelCache.put(0, VALUE1);
        assertEquals(VALUE1, twoLevelCache.get(0));
        assertNull(twoLevelCache.get(111));
    }

    @Test
    public void shouldRemoveDuplicatedObjectFromSecondLevelWhenFirstLevelHasEmptyPlaceTest() throws IOException {
        assertTrue(twoLevelCache.getFirstLevelCache().hasEmptyPlace());

        twoLevelCache.getSecondLevelCache().put(0, VALUE1);
        assertEquals(VALUE1, twoLevelCache.getSecondLevelCache().get(0));

        twoLevelCache.put(0, VALUE1);

        assertEquals(VALUE1, twoLevelCache.getFirstLevelCache().get(0));
        assertFalse(twoLevelCache.getSecondLevelCache().isObjectPresent(0));
    }

    @Test
    public void shouldPutObjectIntoCacheWhenFirstLevelHasEmptyPlaceTest() throws IOException {
        assertTrue(twoLevelCache.getFirstLevelCache().hasEmptyPlace());
        twoLevelCache.put(0, VALUE1);
        assertEquals(VALUE1, twoLevelCache.get(0));
        assertEquals(VALUE1, twoLevelCache.getFirstLevelCache().get(0));
        assertFalse(twoLevelCache.getSecondLevelCache().isObjectPresent(0));
    }

    @Test
    public void shouldPutObjectIntoCacheWhenObjectExistsInFirstLevelCacheTest() throws IOException {
        twoLevelCache.put(0, VALUE1);
        assertEquals(VALUE1, twoLevelCache.get(0));
        assertEquals(VALUE1, twoLevelCache.getFirstLevelCache().get(0));
        assertEquals(1, twoLevelCache.getFirstLevelCache().size());

        // put the same key with other value
        twoLevelCache.put(0, VALUE2);

        assertEquals(VALUE2, twoLevelCache.get(0));
        assertEquals(VALUE2, twoLevelCache.getFirstLevelCache().get(0));
        assertEquals(1, twoLevelCache.getFirstLevelCache().size());
    }

    @Test
    public void shouldPutObjectIntoCacheWhenSecondLevelHasEmptyPlaceTest() throws IOException {
        IntStream.range(0, 1).forEach(i -> {
            try {
                twoLevelCache.put(i, "String " + i);
            } catch (IOException e) {
                log.error("shouldPutObjectIntoCacheWhenSecondLevelHasEmptyPlaceTest", e);
            }
        });

        assertFalse(twoLevelCache.getFirstLevelCache().hasEmptyPlace());
        assertTrue(twoLevelCache.getSecondLevelCache().hasEmptyPlace());

        twoLevelCache.put(2, VALUE2);

        assertEquals(VALUE2, twoLevelCache.get(2));
        assertEquals(VALUE2, twoLevelCache.getSecondLevelCache().get(2));
    }

    @Test
    public void shouldPutObjectIntoCacheWhenObjectExistsInSecondLevelTest() throws IOException {
        IntStream.range(0, 1).forEach(i -> {
            try {
                twoLevelCache.put(i, "String " + i);
            } catch (IOException e) {
                log.error("shouldPutObjectIntoCacheWhenObjectExistsInSecondLevelTest", e);
            }
        });

        assertFalse(twoLevelCache.getFirstLevelCache().hasEmptyPlace());

        twoLevelCache.put(2, VALUE2);

        assertEquals(VALUE2, twoLevelCache.get(2));
        assertEquals(VALUE2, twoLevelCache.getSecondLevelCache().get(2));
        assertEquals(1, twoLevelCache.getSecondLevelCache().size());

        // put the same key with other value
        twoLevelCache.put(2, VALUE3);

        assertEquals(VALUE3, twoLevelCache.get(2));
        assertEquals(VALUE3, twoLevelCache.getSecondLevelCache().get(2));
        assertEquals(1, twoLevelCache.getSecondLevelCache().size());
    }

    @Test
    public void shouldPutObjectIntoCacheWhenObjectShouldBeReplacedTest() throws IOException {
        IntStream.range(0, 2).forEach(i -> {
            try {
                twoLevelCache.put(i, "String " + i);
            } catch (IOException e) {
                log.error("shouldPutObjectIntoCacheWhenObjectShouldBeReplacedTest", e);
            }
        });

        assertFalse(twoLevelCache.hasEmptyPlace());
        assertFalse(twoLevelCache.getStrategy().isObjectPresent(3));

        twoLevelCache.put(3, VALUE3);

        assertTrue(twoLevelCache.get(3).equals(VALUE3));
        assertTrue(twoLevelCache.getStrategy().isObjectPresent(3));
        assertTrue(twoLevelCache.getFirstLevelCache().isObjectPresent(3));
        assertFalse(twoLevelCache.getSecondLevelCache().isObjectPresent(3));
    }

    @Test
    public void shouldsizeTest() throws IOException {
        twoLevelCache.put(0, VALUE1);
        assertEquals(1, twoLevelCache.size());

        twoLevelCache.put(1, VALUE2);
        assertEquals(2, twoLevelCache.size());
    }

    @Test
    public void isObjectPresentTest() throws IOException {
        assertFalse(twoLevelCache.isObjectPresent(0));

        twoLevelCache.put(0, VALUE1);
        assertTrue(twoLevelCache.isObjectPresent(0));
    }

    @Test
    public void isEmptyPlaceTest() throws IOException {
        assertFalse(twoLevelCache.isObjectPresent(0));
        twoLevelCache.put(0, VALUE1);
        assertTrue(twoLevelCache.hasEmptyPlace());

        twoLevelCache.put(1, VALUE2);
        assertFalse(twoLevelCache.hasEmptyPlace());
    }

    @Test
    public void shouldClearCacheTest() throws IOException {
        twoLevelCache.put(0, VALUE1);
        twoLevelCache.put(1, VALUE2);

        assertEquals(2, twoLevelCache.size());
        assertTrue(twoLevelCache.getStrategy().isObjectPresent(0));
        assertTrue(twoLevelCache.getStrategy().isObjectPresent(1));

        twoLevelCache.clearCache();

        assertEquals(0, twoLevelCache.size());
        assertFalse(twoLevelCache.getStrategy().isObjectPresent(0));
        assertFalse(twoLevelCache.getStrategy().isObjectPresent(1));
    }

    @Test
    public void shouldUseLRUStrategyTest() throws IOException {
        twoLevelCache = new Cache<>(1, 1, StrategyType.LRU);
        twoLevelCache.put(0, VALUE1);
        assertEquals(VALUE1, twoLevelCache.get(0));
        assertEquals(VALUE1, twoLevelCache.getFirstLevelCache().get(0));
        assertFalse(twoLevelCache.getSecondLevelCache().isObjectPresent(0));
    }

    @Test
    public void shouldUseMRUStrategyTest() throws IOException {
        twoLevelCache = new Cache<>(1, 1, StrategyType.MRU);
        twoLevelCache.put(0, VALUE1);
        assertEquals(VALUE1, twoLevelCache.get(0));
        assertEquals(VALUE1, twoLevelCache.getFirstLevelCache().get(0));
        assertFalse(twoLevelCache.getSecondLevelCache().isObjectPresent(0));
    }
}

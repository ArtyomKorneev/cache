package cache;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class FileSystemStoreTest {
    private static final Logger log = LoggerFactory.getLogger(FileSystemStoreTest.class);

    private static final String VALUE1 = "value1";
    private static final String VALUE2 = "value2";

    private FileSystemStore<Integer, String> fileSystemCache;

    @Before
    public void init() throws IOException {
        fileSystemCache = new FileSystemStore<>();
    }

    @After
    public void clearCache() throws IOException {
        fileSystemCache.clearCache();
    }

    @Test
    public void shouldPutGetAndRemoveObjectTest() throws IOException {
        fileSystemCache.put(0, VALUE1);
        assertEquals(VALUE1, fileSystemCache.get(0));
        assertEquals(1, fileSystemCache.size());

        fileSystemCache.remove(0);
        assertNull(fileSystemCache.get(0));
    }

    @Test
    public void shouldNotGetObjectFromCacheIfNotExistsTest() throws IOException {
        fileSystemCache.put(0, VALUE1);
        assertEquals(VALUE1, fileSystemCache.get(0));
        assertNull(fileSystemCache.get(111));
    }

    @Test
    public void shouldNotRemoveObjectFromCacheIfNotExistsTest() throws IOException {
        fileSystemCache.put(0, VALUE1);
        assertEquals(VALUE1, fileSystemCache.get(0));
        assertEquals(1, fileSystemCache.size());

        fileSystemCache.remove(5);
        assertEquals(VALUE1, fileSystemCache.get(0));
    }

    @Test
    public void shouldGetCacheSizeTest() throws IOException {
        fileSystemCache.put(0, VALUE1);
        assertEquals(1, fileSystemCache.size());

        fileSystemCache.put(1, VALUE2);
        assertEquals(2, fileSystemCache.size());
    }

    @Test
    public void isObjectPresentTest() throws IOException {
        assertFalse(fileSystemCache.isObjectPresent(0));

        fileSystemCache.put(0, VALUE1);
        assertTrue(fileSystemCache.isObjectPresent(0));
    }

    @Test
    public void isEmptyPlaceTest() throws IOException {
        fileSystemCache = new FileSystemStore<>(5);

        IntStream.range(0, 4).forEach(i -> {
            try {
                fileSystemCache.put(i, "String " + i);
            } catch (IOException e) {
                log.error("isEmptyPlaceTest", e);
            }
        });
        assertTrue(fileSystemCache.hasEmptyPlace());
        fileSystemCache.put(5, "String");
        assertFalse(fileSystemCache.hasEmptyPlace());
    }

    @Test
    public void shouldClearCacheTest() throws IOException {
        IntStream.range(0, 3).forEach((int i) -> {
            try {
                fileSystemCache.put(i, "String " + i);
            } catch (IOException e) {
                log.error("shouldClearCacheTest", e);
            }
        });

        assertEquals(3, fileSystemCache.size());
        fileSystemCache.clearCache();
        assertEquals(0, fileSystemCache.size());
    }
}
